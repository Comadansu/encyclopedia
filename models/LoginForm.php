<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm - модель стоящая за формой входа
 *
 * @property User|null $user доступно только для чтения
 *
 */
class LoginForm extends Model
{
    // Значения приходящие от формы входа
    public $username;
    public $password;
    public $rememberMe = true;

    /**
     * @var bool Экземпляр identity class
     */
    private $_user = false;


    /**
     * @return array Правила валидации
     */
    public function rules()
    {
        return [
            [['username'], 'required', 'message' => 'Имя пользователя не может быть пустым'],
            [['password'], 'required', 'message' => 'Пароль не может быть пустым'],
            // rememberMe должно быть булево значением
            ['rememberMe', 'boolean'],
            // password проверяется с помощью кастомного валидатора validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Валидация пароля
     * Этот метод служит в качестве встроенного валидатора для пароля
     *
     * @param string $attribute Атрибут, который будет проверяться
     * @param array $params Дополнительные пары имя-значение, приведенные в правиле (rule)
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильное имя пользователя или пароль');
            }
        }
    }

    /**
     * Логиним пользователя, используя предоставленные логин и пароль
     * @return boolean если пользователь успешно залогинился
     */
    public function login()
    {
        // Если валидация пройдена, логиним пользователя
        if ($this->validate()) {
            $user = $this->getUser();
            if ($this->validate()) {
                /* Yii\web\User::login() - устанавливает identity текущего пользователя в yii\web\User.
                   Если сессии включены, то identity будет сохраняться в сессии,
                   так что состояние статуса аутентификации будет поддерживаться на всём протяжении сессии.
                   Если основанный на cookie вход (так называемый "запомни меня" вход) включен,
                   то identity также будет сохранена в cookie. Вы также можете настроить время жизни cookie. */
                return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            } else {
                // Неправильный пароль
                return false;
            }
        }
    }

    /**
     * Этот метод находит экземпляр identity class, используя имя пользователя ( User::findByUsername() ).
     *
     * @return User|null Экземпляр identity class
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
