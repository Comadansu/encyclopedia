<?php

namespace app\models;

use yii\data\Pagination;
use yii\data\Sort;
use yii\db\ActiveRecord;

class Encyclopedia extends ActiveRecord
{
    /**
     * @var string Имя таблицы в БД, которое соответствует модели-потомку
     */
    protected static $table = '';

    /**
     * @var array Правила сортировки, соответствующие модели-потомку
     */
    protected static $sort = [];

    public static function tableName()
    {
        return static::$table;
    }

    /**
     * @return Sort Инстанцирование класса Sort
     */
    public static function sort()
    {
        return $sort = new Sort(static::$sort);
    }

    /**
     * @param object $countQuery Результат запроса к БД
     * @param int $perPage Кол-во отображаемых записей на странице
     * @return Pagination Возвращает готовый экземпляр класса Pagination
     */
    public static function pagination($countQuery, $perPage = 10)
    {
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->setPageSize($perPage);
        return $pages;
    }

    /**
     * @param string $flag Флаг, определяющий возвращаемое значение: $countQuery или результирующий массив объектов
     * @param int $perPage Кол-во отображаемых записей на странице
     * @return $this|array|\yii\db\ActiveRecord[]
     */
    public static function findAllItems($flag = 'result', $perPage = 10)
    {
        if ($flag == 'countQuery') {
            $query = self::find()->orderBy(static::sort()->orders);
            $countQuery = clone $query;
            return $countQuery;
        } elseif ($flag == 'result') {
            $query = self::find()->orderBy(static::sort()->orders);
            $countQuery = clone $query;
            $pages = static::pagination($countQuery, $perPage);
            $result = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $result;
        }
    }

    /**
     * @param string $name Имя таблицы
     * @return array|\yii\db\ActiveRecord[] Результирующий массив объектов
     */
    public static function getColumns($name)
    {
        return self::findBySql('SHOW COLUMNS FROM ' . $name)->asArray()->all();
    }

    /**
     * @return mixed Возвращает id последней вставленной записи
     */
    public static function lastInsertId()
    {
        return self::lastInsertId();
    }
}
