<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile Экземпляр загруженного файла
     */
    public $imageFile;

    /**
     * @var string Имя директории с изображениями, соответствующей нужной таблице
     */
    public $dirName;

    /**
     * UploadForm constructor.
     * @param array $config Обязательный родительский параметр
     * @param string $table Имя нужной таблицы
     */
    public function __construct($config = [], $table)
    {
        $this->dirName = $table;

        parent::__construct($config = []);
    }

    /**
     * @return array Правила валидации для загрузки изображений (файл можно не загружать)
     */
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'skipOnError' => false,
                'extensions' => 'png, jpg, ico, gif', 'maxSize' => 1024 * 1024,
                'uploadRequired' => 'Пожалуйста, загрузите файл',
                'wrongExtension' => 'Разрешены файлы только с этими расширениями: {extensions}',
                'message' => 'Файл не удалось загрузить'
            ],
        ];
    }

    /**
     * @return bool Выполняет валидацию и сохраняет загруженный файл на сервере
     */
    public function upload()
    {
        $path = '../web/images/icon/' . $this->dirName . '/';

        if ($this->validate() && $this->imageFile !== null) {
            $this->imageFile->saveAs($path . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
}
