<?php

namespace app\models;

class Book extends Encyclopedia
{
    /**
     * @var string Имя таблицы в БД, которое соответствует этой модели
     */
    public static $table = 'books';

    /**
     * @var array Правила сортировки
     */
    public static $sort = [
        'defaultOrder' => [
            'author' => SORT_ASC
        ],
        'attributes' => [
            'author' => [
                'asc' => ['author' => SORT_ASC],
                'desc' => ['author' => SORT_DESC],
                'default' => SORT_DESC,
                'label' => 'А-Я',
            ],
        ],
    ];
}
