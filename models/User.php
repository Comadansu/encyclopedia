<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * identity class должен реализовывать yii\web\IdentityInterface, который содержит следующие методы:
 * findIdentity(), findIdentityByAccessToken(), getId(), getAuthKey(), validateAuthKey()
 *
 * Class User
 * @package app\models
 */
class User extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }

    /**
     * Этот метод находит экземпляр identity class, используя ID пользователя.
     * Этот метод используется, когда необходимо поддерживать состояние аутентификации через сессии.
     *
     * @inheritdoc
     * @param string|integer $id ID по которому будет производиться поиск
     * @return IdentityInterface|null Объект идентичности, который соответствует данному ID
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Этот метод находит экземпляр identity class, используя токен доступа.
     * Этот метод используется, когда нужно аутентифицировать пользователя по одному секретному токену
     * (например в RESTful приложениях, не сохраняющих состояние между запросами).
     *
     * @inheritdoc
     * @param string $token Токен по которому будет производиться поиск
     * @return IdentityInterface|null Объект идентичности, который соответствует данному токену
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Этот метод находит экземпляр identity class, используя имя пользователя.
     *
     * @param string $username
     * @return IdentityInterface|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Этот метод возвращает ID пользователя, представленного данным экземпляром identity.
     *
     * @inheritdoc
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Этот метод возвращает ключ, используемый для основанной на cookie аутентификации.
     * Ключ сохраняется в аутентификационной cookie и позже сравнивается с версией находящейся на сервере,
     * чтобы удостоверится что аутентификационная cookie верная.
     *
     * @inheritdoc
     * @return string Ключ текущего пользователя
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Этот метод реализует логику проверки ключа для основанной на cookie аутентификации.
     *
     * @inheritdoc
     * @param string $authKey Ключ для проверки
     * @return boolean если auth key действителен для текущего пользователя
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Генерация ключа аутентификации для каждого пользователя (для аутентификации основанной на cookie)
     *
     * @param bool $insert true, если INSERT и false, если UPDATE
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    /**
     * Хеширование пароля перед сохранением в БД
     *
     * @param $pass
     * @return string
     * @throws \yii\base\Exception
     */
    public function setPassword($pass)
    {
        $hash = Yii::$app->getSecurity()->generatePasswordHash($pass);
        return $hash;
    }

    /**
     * Валидация пароля
     *
     * @param string $password Пароль для проверки
     * @return boolean если пароль действителен/недействителен для текущего пользователя
     */
    public function validatePassword($password)
    {
        // Yii::$app->getSecurity()->validatePassword($password, $hash)
        // сравнивает строку (введеный пользователем пароль) и хэш сохраненный в БД
        if (Yii::$app->getSecurity()->validatePassword($password, $this->password)) {
            return true;
        } else {
            return false;
        }
    }
}
