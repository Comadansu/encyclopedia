<?php

namespace app\models;

class Course extends Encyclopedia
{
    /**
     * @var string Имя таблицы в БД, которое соответствует этой модели
     */
    public static $table = 'courses';

    /**
     * @var array Правила сортировки
     */
    public static $sort = [
        'defaultOrder' => [
            'rating' => SORT_DESC
        ],
        'attributes' => [
            'rating' => [
                'asc' => ['rating' => SORT_ASC],
                'desc' => ['rating' => SORT_DESC],
                'default' => SORT_ASC,
                'label' => '<img src="/images/sort-rating.png">',
            ],
        ],
    ];
}
