<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model
{

    // Значения приходящие от регистрационной формы
    public $username;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message' => 'Имя пользователя не может быть пустым'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Такой логин уже используется'],
            ['username', 'string', 'min' => 2, 'tooShort' => 'Логин должен быть больше 2 символов'],
            ['username', 'string', 'max' => 50, 'tooLong' => 'Логин слишком длинный'],

            ['password', 'required', 'message' => 'Пароль не может быть пустым'],
            ['password', 'string', 'min' => 6, 'tooShort' => 'Пароль должен быть больше 6 символов'],
            ['password', 'string', 'max' => 60, 'tooLong' => 'Пароль слишком длинный'],
        ];
    }

    /**
     * Регистрация нового пользователя.
     *
     * @return User|null Сохраненная модель (экземпляр User) или null, если произошла ошибка при сохранении
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $hash = $user->setPassword($this->password);
        $user->password = $hash;
        $user->auth_key;

        return $user->save() ? $user : null;
    }

}
