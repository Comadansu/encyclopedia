<?php

namespace app\models;

use yii\base\Model;

class Form extends Model
{
    /**
     * @var array Массив для хранения динамически созданных свойств
     */
    protected $data = [];

    /**
     * UpdateForm constructor.
     * @param array $config Обязательный родительский параметр
     * @param string $table Имя нужной таблицы
     */
    public function __construct($config = [], $table)
    {
        // Получаем структуру выбранной таблицы
        $columns = Encyclopedia::getColumns($table);
        // Динамически создаем свойства, соответствующие столбцам выбранной таблицы
        foreach ($columns as $key => $value) {
            $this->$key = $value;
        }

        parent::__construct($config = []);
    }

    /**
     * @return array Правила валидации для формы создания/обновления
     */
    public function rules()
    {
        $fields = [];
        foreach ($this->data as $field) {
            if (is_array($field) && $field['Field'] !== 'id' && $field['Field'] !== 'link') {
                $fields[] = $field['Field'];
            }
        }
        return [
            [$fields, 'required', 'message' => 'Это поле не может быть пустым']
        ];
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        return $this->data[$name];
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }
}
