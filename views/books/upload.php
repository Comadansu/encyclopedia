<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\UploadForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Загрузка фотографии';
$this->params['breadcrumbs'][] = ['label' => 'Книги', 'url' => Yii::$app->homeUrl . 'books/index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('FormSubmitted')): ?>

        <div class="alert alert-success">
            Фотография успешно добавлена.
        </div>
        <?php header("refresh: 2; url=index") ?>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                <?= $form->field($model, 'imageFile')->fileInput()->label('Фотография') ?>

                <div class="form-group">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end() ?>

            </div>
        </div>

    <?php endif ?>
</div>
