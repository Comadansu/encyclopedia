<?php
/* @var $this yii\web\View */
/* @var $models \app\controllers\BooksController */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <table style="width: 70%; height: 40px;">
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle;"><strong>Книги</strong></td>
                        <td><a href="create">Создать новую запись</a></td>
                        <td style="font-size: 13px; font-weight: normal; text-align: center; width: 35px; color: #808080; vertical-align: middle; height: 20px;"
                            title="Сортировка по алфавиту"><?= $sort->link('author') ?>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <table cellpadding="5">
                    <tbody id="books">

                    <?php foreach ($models as $book): ?>
                        <!-- Отображаем $book -->
                        <tr>
                            <td style="vertical-align:top;">
                                <?php if (!empty($book->img)): ?>
                                    <a target="_blank" href="<?= $book->link ?>"
                                       title="<?= $book->author . ". " . $book->caption ?>">
                                        <img src="/images/icon/books/<?= $book->img ?>" width="30" height="50">
                                    </a>
                                <?php endif ?>
                            </td>
                            <td style="width: 240px; vertical-align:top;">
                                <strong>&nbsp; <?= $book->author ?> </strong> <br>
                                <?php if (!empty($book->link)): ?>
                                    <a target="_blank" href="<?= $book->link ?>">
                                        &nbsp; <?= $book->caption ?></a>
                                <?php else: ?>
                                    <span>&nbsp; <?= $book->caption ?></span>
                                <?php endif ?>
                            </td>
                            <td style="width: 240px; vertical-align:top;">
                                <a href="<?= 'update?id=' . $book->id ?>">| Изменить</a> <br>
                                <a href="<?= 'upload?id=' . $book->id ?>">| Обновить фото</a> <br>
                                <a href="<?= 'delete?id=' . $book->id ?>"
                                   onclick="return confirm('Вы уверенны?');">| Удалить</a> <br><br>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

                <div>
                    <!-- Отображаем ссылки на страницы -->
                    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]) ?>
                </div>

            </div>

        </div>

    </div>
</div>
