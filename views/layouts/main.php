<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>

    <script src="/assets/ff0e67cf/jquery.min.js"></script>

    <script type="text/javascript">
        /* Функция toogle */
        $(document).ready(function () {
            var flag = new Boolean(true);

            $("#btn3").click(function () {
                if (flag == true) {
                    flag = false;
                    func1.call(this);
                } else {
                    flag = true;
                    func2.call(this);
                }
                return false;
            });

            function func1() {
                $('.minBook').hide();
                $('.maxBook').fadeIn(400);
                $('#minTextBook').hide();
                $('#maxTextBook').show();
                return false;
            }

            function func2() {
                $('.maxBook').hide();
                $('.minBook').fadeIn(400);
                $('#maxTextBook').hide();
                $('#minTextBook').show();
                return false;
            }
        });


        $(document).ready(function () {
            var flag = new Boolean(true);

            $("#btnCourse").click(function () {
                if (flag == true) {
                    flag = false;
                    func1.call(this);
                } else {
                    flag = true;
                    func2.call(this);
                }
                return false;
            });

            function func1() {
                $('.minCourse').hide();
                $('.maxCourse').slideDown(400);
                $('#minTextCourse').hide();
                $('#maxTextCourse').show();
                return false;
            }

            function func2() {
                $('.maxCourse').hide();
                $('.minCourse').slideDown(400);
                $('#maxTextCourse').hide();
                $('#minTextCourse').show();
                return false;
            }
        });
    </script>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <meta property="description"
          content="конференции, фестивали, форумы, мастер-классы, премии, награды, конкурсы, учебные материалы,
          программы и курсы, ассоциации и сообщества рынка заказной веб-разработки и digital-маркетинга,
          издания о digital-маркетинге, веб-разработке и интернет-бизнесе, всё о digital-рынке"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Всё | Энциклопедия digital-рынка',
        'brandUrl' => '/site/index',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $menuItems = [
        ['label' => 'Меню', 'url' => ['/site/index']]
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = (['label' => 'Вход', 'url' => ['/site/login']]);
    } else {
        $menuItems[] = ['label' => 'Регистрация нового пользователя', 'url' => ['/site/signup']];
        $menuItems[] = (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . ')', // Имя текущего аутентифицированного юзера
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>'
        );
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?=
        Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'homeLink' => ['label' => Yii::t('yii', 'Меню'),
                'url' => Yii::$app->homeUrl . 'site/index']
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; 2006−<?= date('Y') ?> Тэглайн / +7 (495) 220-0616 / rating@tagline.ru </p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
