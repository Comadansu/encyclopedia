<?php
/* @var array $model \app\models\Book|\app\models\Course */

$this->title = 'Всё | Энциклопедия digital-рынка — мероприятия, компании, персоны,
    награды, курсы, издания, решения, технологии, ассоциации (Тэглайн)';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <table style="width: 100%; height: 40px;">
                    <tbody>
                    <tr>
                        <td style="vertical-align: middle;"><a
                                href="<?= '/books/index' ?>"><strong>Книги</strong></a></td>
                        <td
                            style="font-size: 13px; font-weight: normal; text-align: center; width: 35px; color: #808080; vertical-align: middle; height: 20px;"
                            title="Сортировка по алфавиту">А-Я&nbsp;<img src="/images/sort-alph.png">
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div id="minBook" class="minBook">
                    <table cellpadding="5">
                        <tbody id="books">
                        <?php $x = 0 ?>
                        <?php foreach ($model["books"] as $book): ?>
                            <?php if ($x == 5) {
                                continue;
                            } ?>
                            <tr>
                                <td style="vertical-align:top;">
                                    <?php if (!empty($book->img)): ?>
                                        <a target="_blank" href="<?= $book->link ?>"
                                           title="<?= $book->author . ". " . $book->caption ?>">
                                            <img src="/images/icon/books/<?= $book->img ?>" width="30"
                                                 height="50">
                                        </a>
                                    <?php endif ?>
                                </td>
                                <td style="width: 240px; vertical-align:top;">
                                    <strong>&nbsp; <?= $book->author ?> </strong> <br>
                                    <?php if (!empty($book->link)): ?>
                                        <a target="_blank" href="<?= $book->link ?>">
                                            &nbsp; <?= $book->caption ?></a>
                                    <?php else: ?>
                                        <span>&nbsp; <?= $book->caption ?></span>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <?php $x++ ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <br>
                </div>

                <div id="maxBook" class="maxBook" style="display: none;">
                    <table cellpadding="5">
                        <tbody id="books">
                        <?php foreach ($model["books"] as $book): ?>
                            <tr>
                                <td style="vertical-align:top;">
                                    <?php if (!empty($book->img)): ?>
                                        <a target="_blank" href="<?= $book->link ?>"
                                           title="<?= $book->author . ". " . $book->caption ?>">
                                            <img src="/images/icon/books/<?= $book->img ?>" width="30"
                                                 height="50">
                                        </a>
                                    <?php endif ?>
                                </td>
                                <td style="width: 240px; vertical-align:top;">
                                    <?php if (!empty($book->link)): ?>
                                        <strong>&nbsp; <?= $book->author ?> </strong> <br>
                                        <a target="_blank" href="<?= $book->link ?>">
                                            &nbsp; <?= $book->caption ?></a>
                                    <?php else: ?>
                                        <span>&nbsp; <?= $book->caption ?></span>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <br>
                </div>
                <div class="HideShowButton">
                    <a href="#" id="btn3" title="">
                        <span id="minTextBook" class="btn btn-default">
                            &nbsp;&nbsp;&nbsp;Показать ещё&nbsp;&nbsp;&nbsp;
                        </span>
                        <span id="maxTextBook" class="btn btn-default" style="display: none;">
                            &nbsp;&nbsp;&nbsp;Скрыть&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </div>
            </div>


            <div class="col-lg-4">
                <table style="width: 100%; height: 40px;">
                    <tbody>
                    <tr>
                        <td style="vertical-align: center;"><a
                                href="<?= '/courses/index' ?>"><strong>Курсы</strong></a></td>
                        <td style="width: 16px;" title="Сортировка по рейтингу"><img src="/images/sort-rating.png"></td>
                    </tr>
                    </tbody>
                </table>

                <div id="minCourse" class="minCourse">
                    <table cellpadding="5">
                        <tbody id="course">
                        <?php $x = 0 ?>
                        <?php foreach ($model["courses"] as $course): ?>
                            <?php if ($x == 3) {
                                continue;
                            } ?>
                            <tr>
                                <?php if (!empty($course->img)): ?>
                                    <td><img src="/images/icon/courses/<?= $course->img ?>" width="16"
                                             height="16"></td>
                                <?php else: ?>
                                    <td></td>
                                <?php endif ?>
                                <td>
                                    <?php if (!empty($course->link)): ?>
                                        <a target="_blank" href="<?= $course->link ?>">
                                            &nbsp; <?= $course->name ?></a>
                                    <?php else: ?>
                                        <span>&nbsp; <?= $course->name ?></span>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <?php $x++ ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <br>
                </div>

                <div id="maxCourse" class="maxCourse" style="display: none;">
                    <table cellpadding="5">
                        <tbody id="course">
                        <?php foreach ($model["courses"] as $course): ?>
                            <tr>
                                <?php if (!empty($course->img)): ?>
                                    <td><img src="/images/icon/courses/<?= $course->img ?>" width="16"
                                             height="16"></td>
                                <?php else: ?>
                                    <td></td>
                                <?php endif ?>
                                <td>
                                    <?php if (!empty($course->link)): ?>
                                        <a target="_blank" href="<?= $course->link ?>">
                                            &nbsp; <?= $course->name ?></a>
                                    <?php else: ?>
                                        <span>&nbsp; <?= $course->name ?></span>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                    <br>
                </div>
                <div class="HideShowButton">
                    <a href="#" id="btnCourse" title="">
                        <span id="minTextCourse" class="btn btn-default">
                            &nbsp;&nbsp;&nbsp;Показать ещё&nbsp;&nbsp;&nbsp;
                        </span>
                        <span id="maxTextCourse" class="btn btn-default" style="display: none;">
                            &nbsp;&nbsp;&nbsp;Скрыть&nbsp;&nbsp;&nbsp;
                        </span>
                    </a>
                </div>

            </div>

        </div>

    </div>
</div>
