<?php
/* @var $this yii\web\View */
/* @var $models \app\controllers\CoursesController */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <table style="width: 70%; height: 40px;">
                    <tbody>
                    <tr>
                        <td style="vertical-align: center;"><strong>Курсы</strong></td>
                        <td><a href="create">Создать новую запись</a></td>
                        <td style="width: 16px;"
                            title="Сортировка по рейтингу"><?= $sort->link('rating') ?></td>
                    </tr>
                    </tbody>
                </table>
                <table cellpadding="5">
                    <tbody id="books">

                    <?php foreach ($models as $course): ?>
                        <!-- Отображаем $course -->
                        <tr>
                            <?php if (!empty($course->img)): ?>
                                <td><img src="/images/icon/courses/<?= $course->img ?>" width="16" height="16">
                                </td>
                            <?php else: ?>
                                <td></td>
                            <?php endif ?>
                            <td>
                                <?php if (!empty($course->link)): ?>
                                    <a target="_blank" href="<?= $course->link ?>">
                                        &nbsp; <?= $course->name ?></a>
                                <?php else: ?>
                                    <span>&nbsp; <?= $course->name ?></span>
                                <?php endif ?>
                            </td>
                            <td style="width: 240px; vertical-align:top;">
                                <a href="<?= 'update?id=' . $course->id ?>">&nbsp; | Изменить</a> <br>
                                <a href="<?= 'upload?id=' . $course->id ?>">&nbsp; | Обновить фото</a> <br>
                                <a href="<?= 'delete?id=' . $course->id ?>"
                                   onclick="return confirm('Вы уверенны?');">&nbsp; | Удалить</a> <br><br>
                            </td>
                        </tr>
                    <?php endforeach ?>

                    </tbody>
                </table>

                <div>
                    <!-- Отображаем ссылки на страницы -->
                    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]) ?>
                </div>

            </div>

        </div>

    </div>
</div>
