<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Form|\app\models\UploadForm */
/* @var $id \app\models\Course->id */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Создание новой записи';
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => Yii::$app->homeUrl . 'courses/index'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('FormSubmitted')): ?>

        <div class="alert alert-success">
            Запись успешно добавлена.
        </div>

        <div class="alert alert-info">
            Загрузите фотографию.
        </div>

        <?php $form = ActiveForm::begin(['action' => 'upload?id=' . $id,
            'options' => ['enctype' => 'multipart/form-data']]) ?>
        <?= $form->field($model, 'imageFile')->fileInput()->label('Фотография') ?>
        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end() ?>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'form']) ?>

                <?php foreach ($model['data'] as $field): ?>
                    <?php if (is_array($field) && $field['Field'] !== 'id' && $field['Field'] !== 'img'): ?>
                        <?= $form->field($model, $field['Field']) ?>
                    <?php endif ?>
                <?php endforeach ?>

                <div class="form-group">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end() ?>

            </div>
        </div>

    <?php endif ?>
</div>
