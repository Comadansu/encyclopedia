<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => ['log'],
    // Домашняя страница по-умолчанию (goHome, homeUrl...)
    // 'homeUrl' => '/encyclopedia/web/',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '3RlGPFCbiSmY89P-1o3za3lqLtT4T6UI',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        // Компонент user управляет статусом аутентификации пользователя.
        // Он требует, чтобы вы указали identity class, который будет содержать текущую логику аутентификации.
        'user' => [
            'identityClass' => 'app\models\User',
            // Если основанный на cookie вход (так называемый "запомни меня" вход) включен,
            // то identity, помимо сохранения в сессии (если сессии включены), также будет сохранена в cookie так,
            // чтобы статус аутентификации пользователя мог быть восстановлен на протяжении всего времени жизни cookie.
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
