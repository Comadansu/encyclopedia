<?php

namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\Book;
use app\models\Course;

class SiteController extends Encyclopedia
{
    /**
     * Отображение главной страницы
     *
     * @return string
     */
    public function actionIndex()
    {
        $model["books"] = Book::findAllItems();
        $model["courses"] = Course::findAllItems();
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Действие для страницы аутентификации
     *
     * @return string
     */
    public function actionLogin()
    {
        // Логиним пользователя:
        $model = new LoginForm();
        // login() используется кастомный
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Yii::$app->homeUrl . 'site/index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Действие для выхода из системы.
     * Метод сбрасывает статус аутентификации сразу и из памяти и из сессии.
     * И по умолчанию, будут также уничтожены все сессионные данные пользователя.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(Yii::$app->homeUrl . 'site/index');
    }

    /**
     * Действие для регистрации нового пользователя.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                // if (Yii::$app->getUser()->login($user)) {
                return $this->redirect(Yii::$app->homeUrl . 'site/index');
                // }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
