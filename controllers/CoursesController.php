<?php

namespace app\controllers;

use app\models\Course;

class CoursesController extends Encyclopedia
{
    /**
     * @var string Имя модели, которая соответствует выбранному контроллеру
     */
    protected static $name = 'app\models\Course';

    /**
     * @return string Действие для отображения страницы "Курсы"
     */
    public function actionIndex()
    {
        $pages = Course::pagination(Course::findAllItems('countQuery'), 7);
        $models = Course::findAllItems('result', 7);
        $sort = Course::sort();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'sort' => $sort
        ]);
    }
}
