<?php

namespace app\controllers;

use app\models\Book;

class BooksController extends Encyclopedia
{
    /**
     * @var string Имя модели, которая соответствует выбранному контроллеру
     */
    protected static $name = 'app\models\Book';

    /**
     * @return string Действие для отображения страницы "Книги"
     */
    public function actionIndex()
    {
        $pages = Book::pagination(Book::findAllItems('countQuery'), 7);
        $models = Book::findAllItems('result', 7);
        $sort = Book::sort();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'sort' => $sort
        ]);
    }
}
