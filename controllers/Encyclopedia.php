<?php

namespace app\controllers;

use yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Form;
use app\models\UploadForm;

class Encyclopedia extends Controller
{
    /**
     * @var string Имя модели (+ пространство имен), которая соответствует выбранному контроллеру
     */
    protected static $name;

    /**
     * Правила доступа ACF ('access control filter')
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                // AccessControl::className() - текущий класс при позднем статическом связывании
                'class' => AccessControl::className(),
                // denyCallback задаёт PHP колбек, который будет вызван, если доступ будет запрещён при вызове этого правила
                'denyCallback' => function ($rule, $action) {
                    if (Yii::$app->user->isGuest) {
                        Yii::$app->user->loginRequired();
                    } else {
                        throw new HttpException(403, 'У вас нет доступа к этой странице');
                    }
                },
                // only указывает, что фильтр ACF нужно применять только к действиям login и logout
                'only' => ['login', 'signup', 'logout', 'index', 'create', 'update', 'upload', 'delete'],
                // rules задаёт правила доступа, которые означают следующее:
                'rules' => [
                    // Разрешить всем гостям (ещё не прошедшим авторизацию) доступ к действию login.
                    // Опция roles содержит знак вопроса ?, это специальный токен обозначающий "гостя".
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    // Разрешить аутентифицированным пользователям доступ к действию logout.
                    // Символ @ это другой специальный токен обозначающий аутентифицированного пользователя.
                    [
                        'actions' => ['logout', 'signup', 'index', 'create', 'update', 'upload', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // verbs (VerbFilter): задаёт http метод (например GET, POST) соответствующий указанному действию.
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Этот код задаёт действие `error` используя класс [[yii\web\ErrorAction]], который рендерит ошибку используя отображение `error`.
     *
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string Имя класса-модели отдельно от пространства имен
     */
    private function getName()
    {
        $rc = new \ReflectionClass(static::$name);
        $pc = $rc->getParentClass();
        if ($pc->getName() == 'app\models\Encyclopedia') {
            // Ищет последний элемент в пространстве имен
            preg_match('/\\\([a-z]+)$/i', $rc->getName(), $matches);
            return $matches[1];
        }
    }

    /**
     * Действие для формы загрузки изображений
     *
     * @return string|yii\web\Response
     */
    public function actionUpload()
    {
        $name = static::$name;
        $request = Yii::$app->request;
        $upload = new UploadForm([], $name::$table);

        if ($request->isPost) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            $item = $name::findOne($request->get('id'));
            // Удаляем прежний файл из файловой системы, если есть
            if ($upload->imageFile != null && $item->img != null) {
                if (file_exists(Yii::getAlias('@webroot/images/icon/') . $upload->dirName . '/' . $item->img)) {
                    unlink(Yii::getAlias('@webroot/images/icon/') . $upload->dirName . '/' . $item->img);
                }
            }
            // Сохраняем новый файл в файловой системе
            if ($upload->upload()) {
                // Сохраняем новый файл в БД
                $fileName = $upload->imageFile->baseName . '.' . $upload->imageFile->extension;
                $item->img = $fileName;
                $item->save();

                Yii::$app->session->setFlash('FormSubmitted');
                return $this->refresh();
            }
            $this->redirect(Yii::$app->homeUrl . strtolower($this->getName()) . 's/index');
        }
        return $this->render('upload', ['model' => $upload]);
    }

    /**
     * Действие для формы создания новой записи
     *
     * @return string
     */
    public function actionCreate()
    {
        $name = static::$name;
        $request = Yii::$app->request;
        $item = new $name;
        $form = new Form([], $name::$table);

        if ($form->load($request->post())) {
            $post = $_POST['Form'];
            foreach ($post as $key => $value) {
                $item->$key = trim($value);
            }
            $item->save();

            $upload = new UploadForm([], $name::$table);
            Yii::$app->session->setFlash('FormSubmitted');
            return $this->render('create', [
                'model' => $upload, 'id' => $item->id
            ]);
        } else {
            foreach ($item as $key => $value) {
                $form->$key = $value;
            }
            return $this->render('create', [
                'model' => $form
            ]);
        }
    }

    /**
     * Действие для формы обновления существующей записи
     *
     * @return string|yii\web\Response
     */
    public function actionUpdate()
    {
        $name = static::$name;
        $request = Yii::$app->request;
        $item = $name::findOne($request->get('id'));
        $form = new Form([], $name::$table);

        if ($form->load($request->post())) {
            $post = $_POST['Form'];
            foreach ($post as $key => $value) {
                $item->$key = trim($value);
            }
            $item->save();
            Yii::$app->session->setFlash('FormSubmitted');
            return $this->refresh();
        } else {
            foreach ($item as $key => $value) {
                $form->$key = $value;
            }
            return $this->render('update', [
                'model' => $form
            ]);
        }
    }

    /**
     * Действие для удаления записи
     *
     * @return yii\web\Response
     */
    public function actionDelete()
    {
        $name = static::$name;
        $item = $name::findOne(Yii::$app->request->get('id'));
        // Если запись имела изображение, удаляем его перед удалением самой записи
        if (file_exists(Yii::getAlias('@webroot/images/icon/') . $name::$table . '/' . $item->img)) {
            unlink(Yii::getAlias('@webroot/images/icon/') . $name::$table . '/' . $item->img);
        }
        $item->delete();
        return $this->redirect($_SERVER["HTTP_REFERER"]);
    }
}
